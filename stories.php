<div class="col-12">
  <h2 class="our-identity text-center">
    Our Stories
  </h2>
  <div class="col-6 offset-3">
    <p class="text-justify mt-2 lead">
      We are so excited and proud of our games. It's really easy to create an obvisioly fun for your awesome days.
    </p>
  </div>
</div>
<div class="col-12">
  <div class="container">
    <div class="row justify-content-md-center mt-3">
      <div class="card-columns">
        <?php for($i=1;$i<=6;$i++) : ?>
          <div class="card-group">
            <div class="card">
              <div style="height: 100%; background-image: url(./images/games/<?php echo $i ?>); background-position: center center; background-size: cover;"></div>
            </div>
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"><?php ritmor('Card title') ?></h5>
                <p class="card-text"><?php ritmor('This card has supporting text below as a natural lead-in to additional content. This card has supporting text below as a natural lead-in to additional content. This card has supporting text below as a natural lead-in to additional content.') ?></p>
              </div>
              <div class="card-footer">
                <small class="text-muted">Last updated 3 mins ago</small>
              </div>
            </div>
          </div>
        <?php endfor; ?>
      </div>

      <button class="btn btn-sm btn-outline-light btn-round w-150 mt-2">Load More</button>
    </div>
  </div>
</div>
