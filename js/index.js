jQuery(document).ready(function($) {
    AOS.init();
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    var elements = document.getElementsByClassName('typewrite');
    for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
          new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);

    $(window).scroll(function () {
        if (window.scrollY < 625) {
           $('nav').fadeOut('slow');
        } else {
           $('nav').fadeIn('slow');
        }
    });

    $("a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $.scrollify.move(hash);
        }
    });

    $('#feature-one').hover(function() {
        $('#baby').fadeOut(400, function() {
          $('#child').fadeIn(400, function() {
          })
        });
    });
    $('#feature-two').hover(function() {
        $('#nobitten').fadeOut(400, function() {
          $('#bitten').fadeIn(400, function() {
          })
        });
    });
    $('#feature-three').hover(function() {
        $('#dragon').fadeOut(400, function() {
          $('#naga').fadeIn(400, function() {
          })
        });
    });
    $('#feature-four').hover(function() {
        $('#asia').fadeOut(400, function() {
          $('#astronaut').fadeIn(400, function() {
          })
        });
    });
    $('#feature-five').hover(function() {
        $('#mobile').fadeOut(400, function() {
          $('#mobilealt').fadeIn(400, function() {
          })
        });
    });

    $("img").click(function (e) {

      // Remove any old one
      $(".ripple").remove();

      // Setup
      var posX = $('div[id=forRipple]').offset().left,
          posY = $('div[id=forRipple]').offset().top,
          buttonWidth = $('div[id=forRipple]').width(),
          buttonHeight =  $('div[id=forRipple]').height();

      // Add the element
      $('div[id=forRipple]').prepend("<span class='ripple'></span>");

     // Make it round!
      if(buttonWidth >= buttonHeight) {
        buttonHeight = buttonWidth;
      } else {
        buttonWidth = buttonHeight;
      }

      // Get the center of the element
      var x = e.pageX - posX - buttonWidth / 2;
      var y = e.pageY - posY - buttonHeight / 2;
      // Add the ripples CSS and start the animation
      $(".ripple").css({
          width: buttonWidth,
          height: buttonHeight,
          top: y + 'px',
          left: x + 'px'
        }).addClass("rippleEffect");
    });

    bio = {};
    bio['utomo'] = {
        nama:'Utomo',
        photo:'./images/6.jpg',
        icon:['ward1.png','ward2.png'],
        expertise:'Canvas Impuriter',
        bio:'Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets. Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.'}
    bio['baskara'] = {
        nama:'Baskara',
        photo:'./images/4.jpeg',
        icon:['spear.png'],
        expertise:'Jomblo Keren',
        bio:'Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets. Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.'}
    bio['anita'] = {
        nama:'Anita',
        photo:'./images/1.jpeg',
        icon:['hp.png','mp.png'],
        expertise:'Ballon Blower',
        bio:'Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets. Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.'}
    bio['emily'] = {
        nama:'Emily',
        photo:'./images/3.jpeg',
        icon:['coach.png'],
        expertise:'The Brain',
        bio:'Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets. Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.'}
    bio['syafri'] = {
        nama:'Syafri',
        photo:'./images/5.jpeg',
        icon:['shield.png'],
        expertise:'Hungriest',
        bio:'Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets. Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.'}
    bio['joko'] = {
        nama:'Joko',
        photo:'./images/2.jpeg',
        icon:['sword.png'],
        expertise:'Web Beggar',
        bio:'Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets. Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.Redantium, totam rem aperiam, eaque ipsa qu ab illo' +
        'inventore veritatis et quasi architectos beatae vitae dicta' +
        'sunt explicaboemo enimse ets.'}

    arrayBio = ['utomo','baskara','anita','emily','syafri','joko'];
    bioIdx = 0;
    $('#utomo,#baskara,#anita,#emily,#syafri,#joko').click(function(){
        sessionStorage.current = '#'+this.id+'OV';
        $('body').css('overflow-y', 'hidden');
        $(sessionStorage.current).removeClass('inactive-overlay');
        $(sessionStorage.current).addClass('active-overlay');
        $('.overlay').fadeIn('slow');
        $('.biograph').fadeOut('slow', function () {$('.biograph').fadeIn()});
        $('#name').text(bio[this.id].nama);
        $('#title').text(bio[this.id].expertise);
        $('#photo').prop('src',bio[this.id].photo);
        $('.roles').remove();
        for (var len = 0; len < bio[this.id].icon.length; len++) {
            $('<img src="./images/'+bio[this.id].icon[len]+'" class="roles" style="top:60%;left:-'+(9-3*len)+'%;width:150px;position:absolute;transform:rotate('+(len*20)+'deg);">').insertAfter('#photo')
        }
        $('#bio').text(bio[this.id].bio);
        bioIdx = arrayBio.indexOf(this.id);
    })
    $('#next').click(function(){
        sessionStorage.old = '#'+arrayBio[bioIdx]+'OV';
        $(sessionStorage.old).removeClass('active-overlay');
        $(sessionStorage.old).addClass('inactive-overlay');
        bioIdx = bioIdx + 1; bioIdx = bioIdx % arrayBio.length;
        sessionStorage.current = '#'+arrayBio[bioIdx]+'OV';
        $(sessionStorage.current).removeClass('inactive-overlay');
        $(sessionStorage.current).addClass('active-overlay');
        $('#biograph').fadeOut('400', function () {$('#biograph').fadeIn()});
        $('#name').text(bio[arrayBio[bioIdx]].nama);
        $('#title').text(bio[arrayBio[bioIdx]].expertise);
        $('#photo').prop('src',bio[arrayBio[bioIdx]].photo);
        $('.roles').remove();
        for (var len = 0; len < bio[arrayBio[bioIdx]].icon.length; len++) {
            $('<img src="./images/'+bio[arrayBio[bioIdx]].icon[len]+'" class="roles" style="top:60%;left:-'+(9-3*len)+'%;width:150px;position:absolute;transform:rotate('+(len*20)+'deg);">').insertAfter('#photo')
        }
        $('#bio').text(bio[arrayBio[bioIdx]].bio);
    })
    $('#prev').click(function(){
        sessionStorage.old = '#'+arrayBio[bioIdx]+'OV';
        $(sessionStorage.old).removeClass('active-overlay');
        $(sessionStorage.old).addClass('inactive-overlay');
        if (bioIdx === 0) bioIdx = arrayBio.length; bioIdx = bioIdx - 1;
        sessionStorage.current = '#'+arrayBio[bioIdx]+'OV';
        $(sessionStorage.current).removeClass('inactive-overlay');
        $(sessionStorage.current).addClass('active-overlay');
        $('#biograph').fadeOut('400', function () {$('#biograph').fadeIn()});
        $('#name').text(bio[arrayBio[bioIdx]].nama);
        $('#title').text(bio[arrayBio[bioIdx]].expertise);
        $('#photo').prop('src',bio[arrayBio[bioIdx]].photo);
        $('.roles').remove();
        for (var len = 0; len < bio[arrayBio[bioIdx]].icon.length; len++) {
            $('<img src="./images/'+bio[arrayBio[bioIdx]].icon[len]+'" class="roles" style="top:60%;left:-'+(9-3*len)+'%;width:150px;position:absolute;transform:rotate('+(len*20)+'deg);">').insertAfter('#photo')
        }
        $('#bio').text(bio[arrayBio[bioIdx]].bio);
    })
    $('#close').click(function(){
        $('.overlay').fadeOut();
        $('body').css('overflow-y', 'inherit');
        $(sessionStorage.ov).removeClass('active-overlay');
        $(sessionStorage.ov).addClass('inactive-overlay');
        sessionStorage.removeItem('current')
        sessionStorage.removeItem('old')
    })
    $(function() {
      $.scrollify({
        section : "section",
        sectionName : "section-name",
        interstitialSection : "",
        easing: "easeOutExpo",
        scrollSpeed: 1100,
        offset : 0,
        scrollbars: false,
        standardScrollElements: "",
        setHeights: true,
        overflowScroll: true,
        updateHash: true,
        touchScroll:true,
        before:function() {},
        after:function() {},
        afterResize:function() {},
        afterRender:function() {}
      });
    });
    $('#vertical').lightSlider({
      gallery:true,
      item:1,
      vertical:true,
      verticalHeight:295,
      vThumbWidth:50,
      thumbItem:8,
      thumbMargin:4,
      slideMargin:0
    });  
});
