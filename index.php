<?php include 'helper.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>SF</title>
  	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/all.min.css">
    <link rel="stylesheet" href="./css/aos.css" />
    <link rel="stylesheet" href="./css/lightslider.min.css" />
	<link rel="stylesheet" href="./css/index.css">
</head>
<body>
	<section class="panel" data-section-name="home">
		<div class="stars"></div>
		<div class="twinkling"></div>
		<div class="clouds"></div>
		<div class="first">
	    	<nav class="navbar navbar-expand-lg fixed-top navbar-light pt-3" id="main" style="display: none">
			  <a class="navbar-brand" href="#"><strong>Ningrat</strong><span>Games</span></a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav ml-auto">
			      <li class="nav-item active">
			        <a class="nav-link" href="#home"><strong>HOME <span class="sr-only">(current)</span></strong></a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="#identity"><strong>IDENTITY</strong></a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="#portofolio"><strong>PORTOFOLIO</strong></a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="#stories"><strong>STORIES</strong></a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="#contact"><strong>CONTACT</strong></a>
			      </li>
			    </ul>
			  </div>
			</nav>
			<div class="container">
				<div class="col-12">
					<h1 class="we-are text-center">
						<div id="logo" class="pb-3">
							<img src="./images/logo.png">
						</div>
						<strong>We are</strong>
						<span class="typewrite" data-period="1000" data-type='[ "gamers.", "Game Developer.", "Joker.", "Bottlestory.", "Ningrat Games!" ]'>
							<span class="wrap"></span>
						</span>
					</h1>
					<h4 class="text-center mt-5">
						Well, we just <strong>started</strong>. <br>
						High hopes aiming for accuracy and <strong>happiness</strong>. <br>
						A qualified team is a <strong>gurantee</strong> for <i>android games</i> <strong>boring but addictive <br>
						</strong> made by <strong>us</strong>.
					</h4>
          <hr class="hr-title mt-4">
          <center>
            <a class="btn btn-xl btn-round btn-outline-light w-250 mt-3" id="acquainted" href="#identity">Get Acquainted</a>
            <br>
            <span class="opacity-60 small-3">Established 2K18</span>
          </center>
				</div>
			</div>
		</div>
	</section>
	<section class="panel second" data-section-name="identity">
		<?php include 'identity.php'; ?>
	</section>
	<section class="panel third" data-section-name="portofolio">
		<?php include 'portofolio.php'; ?>
	</section>
	<section class="panel fourth" data-section-name="stories">
		<?php include 'stories.php'; ?>
	</section>
	<section class="panel fifth" data-section-name="contact">
		<?php include 'contact.php'; ?>
	</section>
	<script type="text/javascript" src="./js/jquery.js"></script>
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script type="text/javascript">
	    paceOptions = {
	        ajax: false, // disabled
	        document: true, // enabled
	        eventLag: false, // disabled
	        restartOnPushState: false
    	}
    </script>
	<script src="./js/pace.min.js"></script>
	<script src="./js/jquery.scrollify.js"></script>
    <script src="./js/lightslider.min.js"></script>
    <script src="./js/aos.js"></script>
	<script src="./js/index.js"></script>
</body>
</html>
