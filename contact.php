<div class="col-12">
  <h2 class="our-identity text-center">
    Let us known
  </h2>
  <div class="col-6 offset-3">
    <p class="text-justify mt-2 lead">
      We are so excited and proud of our games. It's really easy to create an obvisioly fun for your awesome days.
    </p>
  </div>
</div>
<div class="col-12">
  <div class="container">
    <div class="row justify-content-md-center mt-3">
      <div class="col-4 border-right">
        <div class="contact-content my-3">
          Email <br>
          <i class="fas fa-at"></i> cs@ningratgames.com <br>
        </div>
        <div class="contact-content my-3">
          Telephone <br>
          <i class="fas fa-phone-volume"></i> +68 896 1520 4875 <br>
        </div>
        <div class="contact-content my-3">
          Base Camp <br>
          <i class="fas fa-map-marker-alt"></i> Kelapa Gading Jakarta, Indonesia <br><br>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.103818391872!2d106.8267825697754!3d-6.160753599999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe7e84fcc9b45688e!2sBakmi+Aboen!5e0!3m2!1sen!2sid!4v1547716334867" width="auto" height="auto" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      <form class="col-4 my-3">
        <div class="col">
          <input type="text" class="form-control form-control-sm" placeholder="Name"><br>
        </div>
        <div class="col">
          <input type="text" class="form-control form-control-sm" placeholder="Email"><br>
        </div>
        <div class="col">
          <input type="text" class="form-control form-control-sm" placeholder="How do you know us?"><br>
        </div>
        <div class="col">
          <input type="text" class="form-control form-control-sm" placeholder="Subject"><br>
        </div>
        <div class="col">
          <textarea class="form-control form-control-sm" placeholder="Message" cols="40" rows="5"></textarea><br>
        </div>
        <div class="col">
          <center><button class="btn btn-sm btn-outline-light btn-round w-150 mt-2">Submit</button><center>
        </div>
      </form>
    </div>
  </div>
</div>
