<div class="col-12" id="forRipple">
	<h2 class="our-identity text-center">
		Designed for everyone, everywhere
	</h2>
	<div class="col-6 offset-3">
		<p class="text-justify mt-3 lead">
			Our Android games makes played faster and easier. It's made for folks of all skill levels, devices of all shapes, and ads of all sizes.
		</p>
	</div>
	<div class="row justify-content-md-center">
		<div id="feature-one" class="col-2">
			<div class="col-12 text-center feature-logo">
				<i class="fas fa-baby" id="baby"></i>
				<i class="fas fa-child" style="display: none;" id="child"></i>
			</div>
			<p class="col-12 text-justify">
				Yeah, as you ever heard we are just a baby in this magnificent <strong>world</strong>
			</p>
		</div>
		<div id="feature-two" class="col-2">
			<div class="col-12 text-center feature-logo">
				<i class="fas fa-cookie" id="nobitten"></i>
				<i class="fas fa-cookie-bite" style="display: none;" id="bitten"></i>
			</div>
			<p class="col-12 text-justify">
				As well as we love sweet thing. We are pretty sure the sweet could make people be <strong>happy</strong>
			</p>
		</div>
		<div id="feature-three" class="col-2">
			<div class="col-12 text-center feature-logo">
				<i class="fas fa-dragon" id="dragon"></i>
				<i class="fab fa-d-and-d" style="display: none;" id="naga"></i>
			</div>
			<p class="col-12 text-justify">
				With our indosiar's dragon we drive craziest and burn harder our <strong>creativity</strong>
			</p>
		</div>
		<div id="feature-four" class="col-2">
			<div class="col-12 text-center feature-logo">
				<i class="fas fa-globe-asia" id="asia"></i>
				<i class="fas fa-user-astronaut" style="display: none;" id="astronaut"></i>
			</div>
			<p class="col-12 text-justify">
				from our beloved country which is famous for its multicultur and of course <strong>The BALI</strong>
			</p>
		</div>
		<div id="feature-five" class="col-2">
			<div class="col-12 text-center feature-logo">
				<i class="fas fa-mobile" id="mobile"></i>
				<i class="fas fa-mobile-alt" style="display: none;" id="mobilealt"></i>
			</div>
			<p class="col-12 text-justify">
				a lightweight games made special for <strong>Android enthusiast</strong>
			</p>
		</div>
	</div>
	<div class="row justify-content-md-center pt-3">
		<h2 class="col-12 text-center">
			<hr class="hr-title">
			The ingredients
		</h2>
		<div class="col-6 offset-3 pt-3">
			<img src="./images/6.jpg" class="rounded-circle img-ingredients px-1" id='utomo' alt="">
			<img src="./images/4.jpeg" class="rounded-circle img-ingredients px-1" id='baskara' alt="">
			<img src="./images/1.jpeg" class="rounded-circle img-ingredients px-1" id='anita' alt="">
			<img src="./images/3.jpeg" class="rounded-circle img-ingredients px-1" id='emily' alt="">
			<img src="./images/5.jpeg" class="rounded-circle img-ingredients px-1" id='syafri' alt="">
			<img src="./images/2.jpeg" class="rounded-circle img-ingredients px-1" id='joko' alt="">
		</div>
	</div>
</div>
<div class="overlay" style="display: none">
	<button type="button" class="close" aria-label="Close" id="close">
	  <span aria-hidden="true"><i class="fas fa-times"></i></span>
	</button>
	<a href="#" class="prev" id="prev">
	  <i class="fas fa-caret-left"></i>
	</a>
	<a href="#" class="next" id="next">
	  <i class="fas fa-caret-right"></i>
	</a>
	<div class="row" id="biograph">
		<div class="col-6 offset-3 pt-5">
			<div class="media pt-5">
				<img src="" class="img-fluid rounded mr-3" alt="" id="photo">
				<div class="media-body">
					<h3 id="name"></h3>
					<p class="mt-1 lead" id="title"></p>
					<p class="text-justify mt-2 lead" id="bio"></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row justify-content-md-center pt-5">
		<h2 class="col-12 text-center">
			<hr class="hr-title">
			The ingredients
		</h2>
		<div class="col-6 offset-3 pt-3">
			<img src="./images/6.jpg" class="rounded-circle img-ingredients-overlay inactive-overlay px-1" id='utomoOV' alt="">
			<img src="./images/4.jpeg" class="rounded-circle img-ingredients-overlay inactive-overlay px-1" id='baskaraOV' alt="">
			<img src="./images/1.jpeg" class="rounded-circle img-ingredients-overlay inactive-overlay px-1" id='anitaOV' alt="">
			<img src="./images/3.jpeg" class="rounded-circle img-ingredients-overlay inactive-overlay px-1" id='emilyOV' alt="">
			<img src="./images/5.jpeg" class="rounded-circle img-ingredients-overlay inactive-overlay px-1" id='syafriOV' alt="">
			<img src="./images/2.jpeg" class="rounded-circle img-ingredients-overlay inactive-overlay px-1" id='jokoOV' alt="">
		</div>
	</div>
</div>
